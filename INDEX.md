# UNFORMAT

Unformat a disk that you accidentally formatted.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## UNFORMAT.LSM

<table>
<tr><td>title</td><td>UNFORMAT</td></tr>
<tr><td>version</td><td>0.8a</td></tr>
<tr><td>entered&nbsp;date</td><td>1999-03-24</td></tr>
<tr><td>description</td><td>Unformat a disk that you accidentally formatted.</td></tr>
<tr><td>keywords</td><td>format, unformat, harddrive,</td></tr>
<tr><td>author</td><td>Brian Reifsnyder &lt;breifsnyde -at- state.pa.us&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Brian Reifsnyder &lt;breifsnyde -at- state.pa.us&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/FDOS/unformat</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Unformat</td></tr>
</table>
